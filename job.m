function sims = job(arg)
% arg, c'est la ligne de le liste pour ce job la

    % construire la grille de parametres
    x0s    = linspace(0, 1, 10);
    ix2s   = linspace(0, 1, 10);
    slopes = linspace(0, 1, 10);
    [X0, IX2] = meshgrid(x0s, ix2s);

    % on dit, comme un convention, que l'arg indique quelle
    % valeur de x0 & ix2 a prendre ici, 
    x0_ici  = X0(arg)
    ix2_ici = IX2(arg)

    % d'autres parametres
    nve = 3                     % nombre de variables d'etat
    tf  = 100                   % ms
    dt  = 0.05                  % ms
    ds  = 10                    % facteur de sous-echantillonage
    ts  = 0:(dt*ds):tf;         % les points du temps
    nic = 10                    % nombre de condicion iniciales 
    Q = 0.001                   % coefficient de bruit

    % il faut alloc'er la memoire quon utilisera, selon les tailles
    % de model (nve), nombre points du temp, nic, et slopes
    sims = zeros(nve, length(ts), nic, length(slopes));

    for i=1:length(slopes)
        slope_ici = slopes(i)
        for j=1:nic
            init = rand(nve, 1);
            tic
            ys = integrate(@pop1, tf, dt, ds, init, x0_ici, ix2_ici, slope_ici);
            fprintf(stdout, 'sim %d, %d a pris %.2f s\n', i, j, toc);
            sims(:, :, j, i) = ys;
        end
    end

    % sauvegarde le workspace dans un format MATLAB 
    % sous un nom unique
    save('-v7', sprintf('job-workspace-%03d.mat', arg))

end % function job


function ys = integrate(dfun, tf, dt, ds, init, Q, x0_ici, ix2_ici, slope_ici)

    % inicialization pour integration
    n_steps = length(0:dt:tf);
    nve = length(init);
    y = zeros(nve, n_steps);
    y(:, 1) = init;

    % boucle d'integration
    for i=1:(n_steps-1)
        y(:, i+1) = y(:, i) + dt*(dfun(y(:, i)) + randn(nve, 1)*Q);
    end

    % sous-echantillonage
    ys = y(:, 1:ds:end);

end % function integrate


function dy = pop1(y)

    % por ejemplo
    dy = -y;

end % function pop1
