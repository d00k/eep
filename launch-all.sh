#!/usr/bin/env bash

# genere un liste de jobs
n_x0s=10
n_ix2=10
seq $((n_x0s * n_ix2)) > job_list.txt

# rassure que il peut etre lancer
chmod +x run_job.sh

# lancer dans la system de jobs
#   -l walltime=3:30:00 -> on donne un limit sur duree du job, walltime=hh:mm:ss
#   --array-param-file job_list.txt -> un list de jobs a mettre
#   ./run_job.sh -> nom du job a tourner, lancer une fois pour chaque ligne de job_list.txt
oarsub -l walltime=3:30:00 --array-param-file job_list.txt ./run_job.sh

